# Ejercicio 1

## Indica la diferencia entre el uso de la instrucción CMD y ENV (Dockerfile).
La instrucción *ENV* en un Dockerfile sirve para declarar variables de entorno mientras que la variable *CMD* indica una instrucción que se ejectuara por defecto al lanzar el contenedor ( si no se le especifica otra instrucción en el comando de lanzamiento)