# Ejercicio 2
## Indica la diferencia entre el uso de la instrucción ADD y COPY (Dockerfile). 

La instrucción *COPY* i la instrucción *ADD* tienen funciones muy parecidas, básicamente las dos introducen archivos de una fuente externa al interior de la imagen. 
La diferencia es que la instrucción *COPY* solo te permite introducir archivos des de el host donde se construye la imagen y simplemente los copia.
La función *ADD* permite introducir-los des de URLs, des de el host y permite descomprimir archivos dentro de la maquina.
