# Ejercicio 3
## Crea un contenedor con las siguientes especificaciones: 
* Utilizar la imagen base NGINX haciendo uso de la versión 1.19.3
* *Al acceder a la URL localhost:8080/index.html aparecerá el mensaje
HOMEWORK 1
* *Persistir el fichero index.html en un volumen llamado static_content 

Primero he definido el Dockerfile de este directorio, que en la imagen de nginx añade el contenido del directorio static_content.

I seguidamente he ejecutado los comandos del script runNginx.sh

```bash
docker volume create static_content
docker build -t custom-nginx:1.0 .
docker run --rm --name=nginx_homework -p 8080:80 -v static_content:/usr/share/nginx/html -d custom-nginx:1.0
```

Se podría arrancar un contenedor con un volumen a un directorio especifico con este comando en un script:

```bash
docker run --rm --name=nginx_homework -p 8080:80 -v "$(pwd)/static_content:/usr/share/nginx/html" -d custom-nginx:1.0
```
