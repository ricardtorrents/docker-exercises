#Ejercicio 4
## Crea una imagen docker a partir de un Dockerfile. Esta aplicación expondrá un servicio en el puerto 8080 y se deberá hacer uso de la instrucción HEALTHCHECK para comprobar si la aplicación está ofreciendo el servicio o por si el contrario existe un problema.
El healthcheck deberá parametrizarse con la siguiente configuración:
* La prueba se realizará cada 45 segundos
* Por cada prueba realizada, se esperará que la aplicación responda en
menos de 5 segundos. Si tras 5 segundos no se obtiene respuesta, se
considera que la prueba habrá fallado
* Ajustar el tiempo de espera de la primera prueba (Ejemplo: Si la aplicación
del contenedor tarda en iniciarse 10s, configurar el parámetro a 15s)
* El número de reintentos será 2. Si fallan dos pruebas consecutivas, el
contenedor deberá cambiar al estado “unhealthy”)

Modificaremos el docker file añadiendo la siguiente instrucción:
```Dockerfile
HEALTHCHECK --interval=45s --timeout=5s --retries=2 \
  CMD curl -f http://localhost:80/ || exit 1
```
Con la opción *interval* se establece cada cuando se ejecuta el test, con el *timeout* se muestra el tiempo de espera a que responday con el *retries*  el nuemero de reintentos antes de establecer el contenedor como unhealthy.
El resultado lo podemos ver ejecutando docker ps a los 45s

```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/D/d/h/answer_exercise_4 (master)> docker ps
CONTAINER ID        IMAGE                                                 COMMAND                  CREATED             STATUS                    PORTS                                            NAMES
57a87622c3ec        custom-nginx:1.0                                      "/docker-entrypoint.…"   46 seconds ago      Up 45 seconds (healthy)   0.0.0.0:8080->80/tcp  
```

Prueba de error (modificando la funcion a una url no existente), podemos ver que en el segundo 46 haun marca com “starting” por que la primera prueba a fallado:

```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/D/d/h/answer_exercise_4 (master)> docker ps
CONTAINER ID        IMAGE                                                 COMMAND                  CREATED             STATUS                             PORTS                                            NAMES
a04716936e38        custom-nginx:1.0                                      "/docker-entrypoint.…"   48 seconds ago      Up 46 seconds (health: starting)   0.0.0.0:8080->80/tcp    


ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/D/d/h/answer_exercise_4 (master)> docker ps
CONTAINER ID        IMAGE                                                 COMMAND                  CREATED              STATUS                          PORTS                                            NAMES
a04716936e38        custom-nginx:1.0                                      "/docker-entrypoint.…"   About a minute ago   Up About a minute (unhealthy)   0.0.0.0:8080->80/tcp  
```
