
#!/bin/sh
docker volume create static_content
docker build -t custom-nginx:1.0 .
docker run --rm --name=nginx_homework -p 8080:80 -v static_content:/usr/share/nginx/html -d custom-nginx:1.0

#Arrancar el container con un volumen en este directorio
#docker run --rm --name=nginx_homework -p 8080:80 -v "$(pwd)/static_content:/usr/share/nginx/html" -d custom-nginx:1.0